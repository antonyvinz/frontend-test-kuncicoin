import { Avatar, Button, Form, List } from "antd";
import Card from "antd/es/card";
import { Col, Row } from "antd/es/grid";
import Input from "antd/es/input";
import { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [username, setUsername] = useState("");
  const [message, setMessage] = useState("");
  const [chatData, setChatData] = useState([] as any);

  useEffect(() => {
    window.addEventListener("storage", () => {
      const newChatData = JSON.parse(localStorage.getItem("chatData") || "[]");
      setChatData(newChatData);
    });
    window.dispatchEvent(new Event("storage"));
    return () => {
      window.removeEventListener("storage", () => {});
    };
  }, []);

  const handleSendMessage = () => {
    const currentChatData = JSON.parse(localStorage.getItem("chatData") || "[]");
    const newChatData = [...currentChatData, { username, message }];
    localStorage.setItem("chatData", JSON.stringify(newChatData));
    setChatData(newChatData);
    setMessage("");
  };

  return (
    <div className="App">
      <Row>
        <Col>
          <Card className="message-Card">
            <List
              className="message-List"
              dataSource={chatData}
              renderItem={(item: any) => (
                <List.Item key={item.username}>
                  <List.Item.Meta
                    avatar={<Avatar>{item.username.slice(0, 1).toUpperCase()}</Avatar>}
                    description={item.username}
                  />
                  <div className="single-chat">{item.message}</div>
                </List.Item>
              )}
            />
          </Card>
        </Col>
        <Col>
          <Card className="chat-Card">
            <Form>
              <Form.Item label="Username">
                <Input
                  value={username}
                  onChange={(e: any) => {
                    setUsername(e.target.value);
                  }}
                  placeholder="Please enter Username"
                />
              </Form.Item>
              <Form.Item label="Message">
                <Input
                  placeholder="Type a message ..."
                  value={message}
                  onChange={(e: any) => {
                    setMessage(e.target.value);
                  }}
                  onPressEnter={() => handleSendMessage()}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  className={
                    message !== "" && username !== ""
                      ? "button-Actions"
                      : "button-Disabled"
                  }
                  onClick={() => handleSendMessage()}
                  disabled={message !== "" && username !== "" ? false : true}
                >
                  Send
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default App;
